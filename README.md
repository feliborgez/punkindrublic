# PunkInDrublic

A very simple, straightforward application that consumes [Punk API](https://punkapi.com/documentation/v2) and displays lots of beers.

## Architecture

Since almost no presentation logic was required and most of the code in view controllers is UI-related, i decided not to create view models, which was my initial idea. Still, responsibilities are separated.

## Dependencies

I decided not to use any third-party solutions, but some would've helped. For example, with [SnapKit](https://github.com/SnapKit/SnapKit) we could go from this:
```swift
scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
```

To this: 
```swift
scrollView.snp.makeConstraints { make in
	make.edges.equalTo(view)
}
```

## Further info

This app was made with:
**Xcode 10.2**, 
**Swift 5**

## Trivia

This app was named after the **NOFX**'s album *Punk In Drublic* (1994), which is an intended pun on *Drunk In Public*. It just feels related to the app.