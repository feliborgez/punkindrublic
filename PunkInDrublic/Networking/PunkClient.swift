//
//  PunkClient.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 21/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
import UIKit

class PunkClient {
    let session: URLSessionProtocol
    let baseURL = "https://api.punkapi.com/v2/beers"
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    /**
     Dispatches a GET request to a given URL and use the provided *decoder* to provide a generic result.
     
     - Parameters:
         - url: The URL to request.
         - decoder: A block of code responsible for returning a result type for a given **Data** object.
         - handler: The handler that receives the final **Result<T, Error>** object.
     
     OBS: The handling is dispatched in the main thread, but it would not apply to an app with heavy operations.
    */
    private func request<T>(url: URL, decoder: @escaping (Data) -> T?, handler: @escaping (Result<T, Error>) -> Void) {
        session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    handler(.failure(error))
                    return
                }
                
                guard let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode else {
                    handler(.failure(PunkError.network))
                    return
                }
                
                guard let data = data, let result = decoder(data) else {
                    handler(.failure(PunkError.decoding))
                    return
                }
                
                handler(.success(result))
            }
        }
        .resume()
    }
    
    func beers(page: Int, callback: @escaping (Result<[Beer], Error>) -> Void) {
        var components = URLComponents(string: baseURL)!
        components.queryItems = [URLQueryItem(name: "page", value: "\(page)")]
        let url = components.url!
        
        request(url: url,
                decoder: { data -> [Beer]? in
                    let decoder = JSONDecoder()
                    let result = try? decoder.decode([Beer].self, from: data)
                    return result
                },
                handler: callback)
    }
    
    func image(url: URL, callback: @escaping (Result<UIImage, Error>) -> Void) {
        request(url: url, decoder: { UIImage(data: $0) }, handler: callback)
    }
}
