//
//  PunkError.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 20/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

enum PunkError: Error {
    case network, decoding
}
