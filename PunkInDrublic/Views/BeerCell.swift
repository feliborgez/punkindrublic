//
//  BeerCell.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 21/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

class BeerCell: UITableViewCell {
    let beerImageView = UIImageView()
    let nameLabel = UILabel()
    let abvLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    func buildLayout() {
        selectionStyle = .none
        
        addSubview(beerImageView)
        addSubview(nameLabel)
        addSubview(abvLabel)
        
        beerImageView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        abvLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // image view
        beerImageView.layer.cornerRadius = 5
        beerImageView.layer.masksToBounds = true
        beerImageView.contentMode = .scaleAspectFit
        beerImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        beerImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        beerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 15).isActive = true
        beerImageView.heightAnchor.constraint(equalTo: beerImageView.widthAnchor).isActive = true
        beerImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        // name label
        nameLabel.numberOfLines = 0
        nameLabel.minimumScaleFactor = 0.7
        nameLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        nameLabel.topAnchor.constraint(equalTo: beerImageView.topAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: beerImageView.rightAnchor, constant: 20).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        nameLabel.bottomAnchor.constraint(lessThanOrEqualTo: beerImageView.bottomAnchor, constant: -20).isActive = true
        
        // ibu label
        abvLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        abvLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10).isActive = true
    }
    
    func configure(with beer: Beer, cache: ImageCacheProtocol) {
        beerImageView.setImage(url: URL(string: beer.image_url)!, cache: cache, key: "\(beer.id)")
        nameLabel.text = beer.name
        abvLabel.text = "Alcoholic content: \(beer.abv)%"
    }
}
