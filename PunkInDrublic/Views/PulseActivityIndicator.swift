//
//  PulseActivityIndicator.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 23/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

class PulseActivityIndicator: UIView {
    private let color: UIColor
    private (set) var isAnimating = false
    
    init(color: UIColor, frame: CGRect) {
        self.color = color
        super.init(frame: frame)
        setup(size: frame.size, color: color)
        backgroundColor = .blue
//        alpha = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(size: CGSize, color: UIColor) {
        let circleSpacing: CGFloat = 2
        let circleSize: CGFloat = (size.width - 2 * circleSpacing) / 3
        let x: CGFloat = (layer.bounds.size.width - size.width) / 2
        let y: CGFloat = (layer.bounds.size.height - circleSize) / 2
        let duration: CFTimeInterval = 0.75
        let beginTime = CACurrentMediaTime()
        let beginTimes: [CFTimeInterval] = [0.12, 0.24, 0.36]
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.2, 0.68, 0.18, 1.08)
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        animation.keyTimes = [0, 0.3, 1]
        animation.timingFunctions = [timingFunction, timingFunction]
        animation.values = [1, 0.3, 1]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        for i in 0..<3 {
            let layer = circle(size: CGSize(width: circleSize, height: circleSize), color: color)
            let frame = CGRect(x:( x + circleSize * CGFloat(i) + circleSpacing * CGFloat(i)) - (size.width / 2),
                               y: y - 100,
                               width: circleSize,
                               height: circleSize)
            animation.beginTime = beginTime + beginTimes[i]
            layer.frame = frame
            layer.add(animation, forKey: "animation")
            self.layer.addSublayer(layer)
        }
    }
    
    private func circle(size: CGSize, color: UIColor) -> CALayer {
        let layer = CAShapeLayer()
        let path = UIBezierPath()
        
        path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                    radius: size.width / 2,
                    startAngle: 0,
                    endAngle: CGFloat(2 * Double.pi),
                    clockwise: false)
        
        layer.fillColor = color.cgColor
//        layer.backgroundColor = nil
        layer.path = path.cgPath
        layer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        return layer
    }
    
    func startAnimating() {
        self.alpha = 1
        isAnimating = true
    }
    
    func stopAnimating() {
        self.alpha = 0
        isAnimating = false
    }
}
