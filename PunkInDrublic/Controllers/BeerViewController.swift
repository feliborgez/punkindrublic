//
//  BeerViewController.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 21/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

class BeerViewController: UIViewController {
    let beer: Beer
    let imageView = UIImageView()
    let abvLabel = UILabel()
    let ibuLabel = UILabel()
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    let taglineLabel = UILabel()
    let scrollView = UIScrollView()
    let textContainer = UIView()
    
    init(beer: Beer) {
        self.beer = beer
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        buildLayout()
        configureInfo()
    }
    
    func buildLayout() {
        
        // text container
        view.addSubview(textContainer)
        textContainer.translatesAutoresizingMaskIntoConstraints = false
        textContainer.backgroundColor = .yellow
        textContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        textContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        textContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        // scroll view
        view.addSubview(scrollView)
        scrollView.alwaysBounceVertical = true
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        // image container
        let imageContainer = UIView()
        imageContainer.translatesAutoresizingMaskIntoConstraints = false
        imageContainer.backgroundColor = .gray
        scrollView.addSubview(imageContainer)
        imageContainer.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        imageContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageContainer.heightAnchor.constraint(equalTo: imageContainer.widthAnchor, multiplier: 0.7).isActive = true
        
        // image view
        scrollView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .white
        let imageViewTopConstraint = imageView.topAnchor.constraint(equalTo: view.topAnchor)
        imageViewTopConstraint.priority = .defaultHigh
        imageViewTopConstraint.isActive = true
        imageView.heightAnchor.constraint(greaterThanOrEqualTo: imageContainer.heightAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: imageContainer.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: imageContainer.rightAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: imageContainer.bottomAnchor).isActive = true
        
        // title label
        scrollView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.backgroundColor = .red
        titleLabel.font = UIFont.systemFont(ofSize: 30, weight: .heavy)
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.topAnchor.constraint(equalTo: imageContainer.bottomAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        // tagline label
        scrollView.addSubview(taglineLabel)
        taglineLabel.translatesAutoresizingMaskIntoConstraints = false
        taglineLabel.textColor = .red
        taglineLabel.font = UIFont.systemFont(ofSize: 25, weight: .light)
        taglineLabel.textAlignment = .center
        taglineLabel.numberOfLines = 0
        taglineLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        taglineLabel.leftAnchor.constraint(equalTo: textContainer.leftAnchor, constant: 20).isActive = true
        taglineLabel.rightAnchor.constraint(equalTo: textContainer.rightAnchor, constant: -20).isActive = true
        
        // description label
        scrollView.addSubview(descriptionLabel)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = .red
        descriptionLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.leftAnchor.constraint(equalTo: textContainer.leftAnchor, constant: 20).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: textContainer.rightAnchor, constant: -20).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: taglineLabel.bottomAnchor, constant: 10).isActive = true
        
        
 
        // abv label
        scrollView.addSubview(abvLabel)
        abvLabel.translatesAutoresizingMaskIntoConstraints = false
        abvLabel.minimumScaleFactor = 0.75
        abvLabel.textColor = .darkRed
        abvLabel.adjustsFontSizeToFitWidth = true
        abvLabel.textAlignment = .center
        abvLabel.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
        abvLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 20).isActive = true
        abvLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        abvLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true


        // ibu label
        scrollView.addSubview(ibuLabel)
        ibuLabel.translatesAutoresizingMaskIntoConstraints = false
        ibuLabel.textAlignment = .center
        ibuLabel.minimumScaleFactor = 0.75
        ibuLabel.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
        ibuLabel.textColor = .darkRed
        ibuLabel.adjustsFontSizeToFitWidth = true
        ibuLabel.topAnchor.constraint(equalTo: abvLabel.bottomAnchor, constant: 20).isActive = true
        ibuLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        ibuLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        ibuLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20).isActive = true
    }
    
    func configureInfo() {
        imageView.setImage(url: URL(string: beer.image_url)!, cache: ImageCache.shared, key: "\(beer.id)")
        titleLabel.text = beer.name
        taglineLabel.text = "\"\(beer.tagline)\""
        descriptionLabel.text = beer.description
        abvLabel.text = "Alcoholic contents: \(beer.abv)%"
        if let ibu = beer.ibu {
            ibuLabel.text = "Bitterness: \(ibu)"
        }
    }
}
