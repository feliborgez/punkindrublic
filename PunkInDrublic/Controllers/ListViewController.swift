//
//  ViewController.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 19/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {
    
    var activityIndicator: PulseActivityIndicator!
    var page = 1
    var client = PunkClient()
    var cache: ImageCacheProtocol = ImageCache.shared
    var beers = [Beer]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupIndicator()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reload), for: .valueChanged)
        self.refreshControl = refreshControl
        
        tableView.register(BeerCell.self, forCellReuseIdentifier: "BeerCell")
        requestBeers()
    }
    
    func setupIndicator() {
        activityIndicator = PulseActivityIndicator(color: .orange, frame: CGRect(x: 0, y: 0, width: 50, height: 10))
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.layer.borderColor = UIColor.black.cgColor
        activityIndicator.layer.borderWidth = 10
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
    }
    
    func requestBeers() {
        client.beers(page: page) { result in
            self.refreshControl?.endRefreshing()
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(let beers):
                self.beers.append(contentsOf: beers)
                self.insert(numberOfBeers: beers.count)
                self.page += 1
            case .failure(_):
                let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Check your internet connection and try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func insert(numberOfBeers: Int) {
        let indexPaths = ((beers.count - numberOfBeers)..<beers.count).map { IndexPath(row: $0 - 1, section: 0) }
        tableView.beginUpdates()
        tableView.insertRows(at: indexPaths, with: .automatic)
        tableView.endUpdates()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeerCell") as! BeerCell
        let beer = beers[indexPath.row]
        cell.configure(with: beer, cache: cache)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == beers.count - 1 {
            requestBeers()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = BeerViewController(beer: beers[indexPath.row])
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func reload() {
        beers = []
        page = 1
        tableView.reloadData()
        requestBeers()
    }
}
