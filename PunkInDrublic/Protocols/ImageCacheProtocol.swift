//
//  ImageCacheProtocol.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 22/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

protocol ImageCacheProtocol {
    func get(key: String) -> UIImage?
    func set(_ image: UIImage, key: String)
}
