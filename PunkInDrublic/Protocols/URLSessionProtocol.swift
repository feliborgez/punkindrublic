//
//  URLSessionProtocol.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 22/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

protocol URLSessionProtocol {
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol { }
