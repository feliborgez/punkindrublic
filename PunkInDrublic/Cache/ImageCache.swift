//
//  Cache.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 20/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
import UIKit

class ImageCache: ImageCacheProtocol {
    
    public static let shared = ImageCache()
    
    private init() { }
    
    private let cache = NSCache<NSString, UIImage>()
    
    func set(_ image: UIImage, key: String) {
        cache.setObject(image, forKey: key as NSString)
    }
    
    func get(key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }
}
