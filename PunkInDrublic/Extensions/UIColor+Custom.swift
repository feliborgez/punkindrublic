//
//  UIColor+Custom.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 24/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

extension UIColor {
    static var darkRed: UIColor {
        return UIColor(red: 139/255, green: 0, blue: 0, alpha: 1)
    }
}
