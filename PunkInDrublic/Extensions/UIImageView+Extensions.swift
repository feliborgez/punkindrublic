//
//  UIImageView+Extensions.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 20/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit

extension UIImageView {
    func setImage(url: URL, cache: ImageCacheProtocol, key: String, placeholder: UIImage? = nil) {
        if let image = cache.get(key: key) {
            self.image = image
            return
        }
        
        self.image = placeholder
        
        let client = PunkClient()
        client.image(url: url) { result in
            switch result {
            case .success(let image):
                cache.set(image, key: key)
                self.image = image
            case .failure:
                break
            }
        }
    }
}
