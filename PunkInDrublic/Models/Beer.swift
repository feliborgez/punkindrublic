//
//  Beer.swift
//  PunkInDrublic
//
//  Created by Felipe Borges  on 19/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

struct Beer: Decodable, Encodable {
    let id: Int
    let name: String
    let ibu: Float?
    let abv: Float
    let image_url: String
    let description: String
    let tagline: String
}
