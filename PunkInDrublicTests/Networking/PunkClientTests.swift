//
//  PunkClientTests.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 22/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import XCTest
@testable import PunkInDrublic

class PunkClientTests: XCTestCase {
    var sut: PunkClient!

    override func tearDown() {
        sut = nil
    }
    
    func testBeers() {
        sut = PunkClient(session: URLSessionMock(mockReturn: .beers))
        
        let successExpectation = expectation(description: "Should return beers successfully.")
        
        sut.beers(page: 1) { result in
            if case let Result.success(beers) = result {
                if !beers.isEmpty { successExpectation.fulfill() }
            }
        }
        
        wait(for: [successExpectation], timeout: 3)
    }
    
    func testImage() {
        sut = PunkClient(session: URLSessionMock(mockReturn: .image))
        
        let successExpectation = expectation(description: "Should return image successfully.")
        
        sut.image(url: URL(string: "https://http.cat/200")!) { result in
            if case Result.success(_) = result {
                successExpectation.fulfill()
            }
        }
        
        wait(for: [successExpectation], timeout: 3)
    }
    
    func testError() {
        sut = PunkClient(session: URLSessionMock(mockReturn: .error))
        
        let errorExpectation = expectation(description: "Should return error.")
    
        sut.beers(page: 1) { result in
            if case let Result.failure(error) = result {
                if case PunkError.decoding = error { errorExpectation.fulfill() }
            }
        }
        
        wait(for: [errorExpectation], timeout: 3)
    }
}
