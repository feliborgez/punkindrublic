//
//  ListViewControllerSpy.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 23/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
@testable import PunkInDrublic

class ListViewControllerSpy: ListViewController {
    var didCallRequestBeers = false
    var didCallInsert = false
    
    override func requestBeers() {
        super.requestBeers()
        didCallRequestBeers = true
    }
    
    override func insert(numberOfBeers: Int) {
        super.insert(numberOfBeers: numberOfBeers)
        didCallInsert = true
    }
}
