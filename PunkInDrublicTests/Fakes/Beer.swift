//
//  Beer.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 22/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation
@testable import PunkInDrublic

extension Beer {
    static func fakeTimes(_ times: Int) -> [Beer] {
        return
            (0..<times).map { i in
                let beer = Beer(id: i, name: "Skol", ibu: 40, abv: 20, image_url: "https://http.cat/200", description: "Traditional brazilian beer.", tagline: "What")
                return beer
            }
    }
}
