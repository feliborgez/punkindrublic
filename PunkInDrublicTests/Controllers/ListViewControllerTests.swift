//
//  ListViewControllerTests.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 23/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import XCTest
@testable import PunkInDrublic

class ListViewControllerTests: XCTestCase {
    var sut: ListViewControllerSpy!

    override func setUp() {
        sut = ListViewControllerSpy()
        sut.client = PunkClient(session: URLSessionMock(mockReturn: .beers))
        sut.cache = ImageCacheMock()
        
    }
    
    func testReload() {
        // When
        _ = sut.view
        sut.reload()
        
        // Then
        XCTAssert(sut.beers.isEmpty)
        XCTAssertEqual(sut.page, 1)
        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), 0)
        XCTAssert(sut.didCallRequestBeers)
    }
    
    func testViewDidLoad() {
        // When
        _ = sut.view
        
        // Then
        XCTAssertNotNil(sut.refreshControl)
        XCTAssert(sut.didCallRequestBeers)
    }
    
    func testInsert() {
        // Given
        let beers = Beer.fakeTimes(20)
        
        // When
        sut.beers = beers
        sut.insert(numberOfBeers: 20)
        
        // Then
        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), 20)
    }
}
