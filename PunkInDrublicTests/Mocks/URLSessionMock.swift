//
//  URLSessionMock.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 22/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit
@testable import PunkInDrublic

class URLSessionMock: URLSessionProtocol {
    private let mockReturn: MockReturn
    
    init(mockReturn: MockReturn) {
        self.mockReturn = mockReturn
    }
    
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return URLSessionDataTaskMock(closure: {
            switch self.mockReturn {
            case .beers:
                completionHandler(try? JSONEncoder().encode(Beer.fakeTimes(20)), HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
            case .image:
                completionHandler(UIImage(named: "longneck-bottle", in: Bundle.main, compatibleWith: nil)!.pngData(), HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
            case .error:
                completionHandler(nil, HTTPURLResponse(url: url, statusCode: 404, httpVersion: nil, headerFields: nil), PunkError.decoding)
            }
        })
    }
}

enum MockReturn {
    case beers, image, error
}
