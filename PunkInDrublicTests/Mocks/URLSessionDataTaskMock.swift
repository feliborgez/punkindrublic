//
//  URLSessionDataTaskMock.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 22/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import Foundation

class URLSessionDataTaskMock: URLSessionDataTask {
    let closure: () -> Void
    
    init(closure: @escaping () -> Void) {
        self.closure = closure
    }
    
    override func resume() {
        closure()
    }
}
