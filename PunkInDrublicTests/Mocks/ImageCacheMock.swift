//
//  ImageCacheMock.swift
//  PunkInDrublicTests
//
//  Created by Felipe Borges  on 23/05/19.
//  Copyright © 2019 Felipe Borges . All rights reserved.
//

import UIKit
@testable import PunkInDrublic

class ImageCacheMock: ImageCacheProtocol {
    private var images: [String: UIImage] = [:]
    
    func get(key: String) -> UIImage? {
        return images[key]
    }
    
    func set(_ image: UIImage, key: String) {
        images[key] = image
    }
}
